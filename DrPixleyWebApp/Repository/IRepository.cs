﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrPixleyWebApp.Models
{
    interface IRepository
    {
        void UploadNewsArticle();
        void UploadNewTenderDocument();

        void DownloadDocument();
    }
}
