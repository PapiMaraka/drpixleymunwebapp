﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DrPixleyWebApp.Startup))]
namespace DrPixleyWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
